<?php
//
// +----------------------------------------------------------------------+
// |zen-cart Open Source E-commerce                                       |
// +----------------------------------------------------------------------+
// | Copyright (c) 2005 Russ Bernhardt - russ@russbernhardt.com           |
// | Portions Copyright (c) 2003 The zen-cart developers                  |
// |                                                                      |
// | http://www.zen-cart.com/index.php                                    | 
// |                                                                      | 
// | Portions Copyright (c) 2003 osCommerce                               |
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.zen-cart.com/license/2_0.txt.                             |
// | If you did not receive a copy of the zen-cart license and are unable |
// | to obtain it through the world-wide-web, please send a note to       |
// | license@zen-cart.com so we can mail you a copy immediately.          |
// +----------------------------------------------------------------------+
// $Id: cs.php,v 1.12 2005/05/30 17:00:19 nquinlan Exp $
//
class cs
 {
  var $code, $title, $description, $enabled, $setfiletime;
  // class constructor
  function CS()
   {
   
    global $order;
	global $setfiletime;
    $this->code        = 'cs';
    $this->title       = MODULE_PAYMENT_CS_TEXT_TITLE;
    $this->description = MODULE_PAYMENT_CS_TEXT_DESCRIPTION;
    $this->enabled     = ((MODULE_PAYMENT_CS_STATUS == 'True') ? true : false);
    $this->sort_order  = MODULE_PAYMENT_CS_SORT_ORDER;
	$this->setfiletime = time();
    if ((int) MODULE_PAYMENT_CS_ORDER_STATUS_ID > 0)
     {
      $this->order_status = MODULE_PAYMENT_CS_ORDER_STATUS_ID;
     }
    if (is_object($order))
      $this->update_status();
    $url = explode('/', $_SERVER['PHP_SELF']);
    if (strcmp($url[2], 'admin') != 0)
     {
      // Make sure that your HOP.php file is located in your store's root
      // directory, i.e., /store/ or /catalog/ etc.
      //include_once("HOP.php");
      //include_once(DIR_FS_CATALOG. DIR_WS_MODULES . 'payment/cybersource/HOP.php');
     }
    // The line below is the production line.
    // $this->form_action_url = 'https://orderpage.ic3.com/hop/ProcessOrder.do';
    // The line below is for debugging and doesn't process the order or
    // return a status. It halts on CyberSource's checker script to show you
    // the data being received and if it's valid.
    //$this->form_action_url = 'https://orderpage.ic3.com/hop/CheckOrderData.do';
    $this->form_action_url = 'https://secureacceptance.cybersource.com/silent/pay';
    if (MODULE_PAYMENT_CS_TESTMODE == "Test")
      $this->form_action_url = 'https://testsecureacceptance.cybersource.com/silent/pay';
   }
  // class methods
  function update_status()
   {
    global $order, $db;
    if (($this->enabled == true) && ((int) MODULE_PAYMENT_CS_ZONE > 0))
     {
      $check_flag = false;
      $check      = $db->Execute("select zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . MODULE_PAYMENT_CS_ZONE . "' and zone_country_id = '" . $order->billing['country']['id'] . "' order by zone_id");
      while (!$check->EOF)
       {
        if ($check->fields['zone_id'] < 1)
         {
          $check_flag = true;
          break;
         }
        elseif ($check->fields['zone_id'] == $order->billing['zone_id'])
         {
          $check_flag = true;
          break;
         }
        $check->MoveNext();
       }
      if ($check_flag == false)
       {
        $this->enabled = false;
       }
     }
   }
  function javascript_validation()
   {
    $js = '  if (payment_value == "' . $this->code . '") {' . "\n" . '    var cc_owner = document.checkout_payment.first_name.value + " " + document.checkout_payment.last_name.value;' . "\n" . '    var cc_number = document.checkout_payment.card_accountNumber.value;' . "\n" . '	   var cc_cvv = document.checkout_payment.card_cvNumber.value;' . "\n" . '    if (cc_owner == "" || cc_owner.length < ' . CC_OWNER_MIN_LENGTH . ') {' . "\n" . '      error_message = error_message + "' . MODULE_PAYMENT_CS_TEXT_JS_CC_OWNER . '";' . "\n" . '      error = 1;' . "\n" . '    }' . "\n" . '    if (cc_number == "" || cc_number.length < ' . CC_NUMBER_MIN_LENGTH . ') {' . "\n" . '      error_message = error_message + "' . MODULE_PAYMENT_CS_TEXT_JS_CC_NUMBER . '";' . "\n" . '      error = 1;' . "\n" . '    }' . "\n";
    if (MODULE_PAYMENT_CS_COLLECT_CVV == 'True')
     {
      $js .= '    if (cc_cvv == "" || cc_cvv.length < ' . CC_CVV_MIN_LENGTH . ') {' . "\n" . '      error_message = error_message + "' . MODULE_PAYMENT_CS_TEXT_JS_CC_CVV . '";' . "\n" . '      error = 1;' . "\n" . '    }' . "\n";
     }
    $js .= '  }' . "\n";
    return $js;
   }
  function selection()
   {
    global $order;
    for ($i = 1; $i < 13; $i++)
     {
      $expires_month[] = array(
        'id' => sprintf('%02d', $i),
        'text' => strftime('%B', mktime(0, 0, 0, $i, 1, 2000))
      );
     }
    $today = getdate();
    for ($i = $today['year']; $i < $today['year'] + 10; $i++)
     {
      $expires_year[] = array(
        'id' => strftime('%y', mktime(0, 0, 0, 1, 1, $i)),
        'text' => strftime('%Y', mktime(0, 0, 0, 1, 1, $i))
      );
     }
    $onFocus   = ' onfocus="methodSelect(\'pmt-' . $this->code . '\')"';
    $selection = array(
      'id' => $this->code,
      'module' => $this->title,
      'fields' => array(
        array(
          'title' => MODULE_PAYMENT_CS_TEXT_CREDIT_CARD_OWNER_FIRST,
          'field' => zen_draw_input_field('first_name', $order->billing['firstname']),
          $onFocus
        ),
        array(
          'title' => MODULE_PAYMENT_CS_TEXT_CREDIT_CARD_OWNER_LAST,
          'field' => zen_draw_input_field('last_name', $order->billing['lastname']),
          $onFocus
        ),
        array(
          'title' => MODULE_PAYMENT_CS_TEXT_CREDIT_CARD_NUMBER,
          'field' => zen_draw_input_field('card_accountNumber', '', $onFocus)
        ),
        array(
          'title' => MODULE_PAYMENT_CS_TEXT_CREDIT_CARD_EXPIRES,
          'field' => zen_draw_pull_down_menu('card_expirationMonth', $expires_month, $onFocus) . '&nbsp;' . zen_draw_pull_down_menu('card_expirationYear', $expires_year, $onFocus)
        )
      )
    );
    if (MODULE_PAYMENT_CS_COLLECT_CVV == 'True')
     {
      $selection['fields'][] = array(
        'title' => MODULE_PAYMENT_CS_TEXT_CREDIT_CARD_CVV,
        'field' => zen_draw_input_field('card_cvNumber', '', $onFocus)
      );
     }
    return $selection;
   }
  function pre_confirmation_check()
   {
    global $_POST, $messageStack;
    include(DIR_WS_CLASSES . 'cc_validation.php');
    $cc_validation = new cc_validation();
    $result        = $cc_validation->validate($_POST['card_accountNumber'], $_POST['card_expirationMonth'], $_POST['card_expirationYear']);
    $error         = '';
    switch ($result)
    {
      case -1:
        $error = sprintf(TEXT_CCVAL_ERROR_UNKNOWN_CARD, substr($cc_validation->cc_number, 0, 4));
        break;
      case -2:
      case -3:
      case -4:
        $error = TEXT_CCVAL_ERROR_INVALID_DATE;
        break;
      case false:
        $error = TEXT_CCVAL_ERROR_INVALID_NUMBER;
        break;
    }
    if (($result == false) || ($result < 1))
     {
      //$payment_error_return = 'payment_error=' . $this->code . '&error=' . urlencode($error) . '&cc_owner=' . urlencode($_POST['cc_owner']) . '&card_expirationMonth=' . $_POST['card_expirationMonth'] . '&card_expirationYear=' . $_POST['card_expirationYear'];
      $messageStack->add_session('checkout_payment', $error, 'error');
      zen_redirect(zen_href_link(FILENAME_CHECKOUT_PAYMENT, $payment_error_return, 'SSL', true, false));
     }
    $this->cc_card_type = $cc_validation->cc_type;
    switch ($this->cc_card_type)
    {
      case "MasterCard":
        $this->cc_card_type_num = "002";
        break;
      case "Visa":
        $this->cc_card_type_num = "001";
        break;
      case "American Express":
        $this->cc_card_type_num = "003";
        break;
      case "Discover":
        $this->cc_card_type_num = "004";
        break;
    }
    $_SESSION['cardcvnumber'] = $_POST['card_cvNumber'];
    $this->cc_card_number     = $cc_validation->cc_number;
    $this->cc_card_first_name = $_POST['first_name'];
    $this->cc_card_last_name  = $_POST['last_name'];
    if (MODULE_PAYMENT_CS_COLLECT_CVV == 'True')
     {
      $this->cc_card_cvNumber = $_POST['card_cvNumber']; //Needed for CVV submissions.
     }
    $this->cc_expiry_month = $cc_validation->cc_expiry_month;
    $this->cc_expiry_year  = $cc_validation->cc_expiry_year;
   }
  function confirmation()
   {
    global $_POST;
    $confirmation = array(
      'title' => $this->title . ': ' . $this->cc_card_type,
      'fields' => array(
        array(
          'title' => MODULE_PAYMENT_CS_TEXT_CREDIT_CARD_OWNER_FIRST,
          'field' => $_POST['first_name']
        ),
        array(
          'title' => MODULE_PAYMENT_CS_TEXT_CREDIT_CARD_OWNER_LAST,
          'field' => $_POST['last_name']
        ),
        array(
          'title' => MODULE_PAYMENT_CS_TEXT_CREDIT_CARD_NUMBER,
          'field' => substr($this->cc_card_number, 0, 4) . str_repeat('X', (strlen($this->cc_card_number) - 8)) . substr($this->cc_card_number, -4)
        ),
        array(
          'title' => MODULE_PAYMENT_CS_TEXT_CREDIT_CARD_EXPIRES,
          'field' => strftime('%B, %Y', mktime(0, 0, 0, $_POST['card_expirationMonth'], 1, '20' . $_POST['card_expirationYear']))
        )
      )
    );
    if (MODULE_PAYMENT_CS_COLLECT_CVV == 'True')
     {
      $confirmation['fields'][] = array(
        'title' => MODULE_PAYMENT_CS_TEXT_CREDIT_CARD_CVV,
        'field' => $_POST['card_cvNumber']
      );
     }
    return $confirmation;
   }
  function process_button()
   {
    global $_SERVER, $db, $order;
    // Calculate the next expected order id
    // modified by Numinix to get next order number
    $qShowStatus       = "SHOW TABLE STATUS LIKE '" . TABLE_ORDERS . "'";
    $qShowStatusResult = mysql_query($qShowStatus);
    $row               = mysql_fetch_assoc($qShowStatusResult);
    $new_order_id      = $row['Auto_increment'];
    if (!($new_order_id > 0))
     {
      $last_order_id = $db->Execute("select * from " . TABLE_ORDERS . " order by orders_id desc limit 1");
      $new_order_id  = $last_order_id->fields['orders_id'];
      $new_order_id  = ($new_order_id + 1);
     }
    if (MODULE_PAYMENT_CS_COLLECT_CVV == 'True')
     {
      // $process_button_string .= zen_draw_hidden_field('card_cvNumber',$this->cc_card_cvNumber) . "\n";//Needed for CVV submissions.
     }
    // MODIFIED BY NUMINIX TO INCLUDE CANADIAN PROVINCES AND TERRITORIES
    $states = array(
      'ARMED FORCES AMERICA' => 'AA',
      'ARMED FORCES EUROPE' => 'AE',
      'BRITISH COLUMBIA' => 'BC',
      'ALASKA' => 'AK',
      'ALABAMA' => 'AL',
      'ALBERTA' => 'AB',
      'ARMED FORCES PACIFIC' => 'AP',
      'ARKANSAS' => 'AR',
      'ARIZONA' => 'AZ',
      'CALIFORNIA' => 'CA',
      'COLORADO' => 'CO',
      'CONNECTICUT' => 'CT',
      'DISTRICT OF COLUMBIA' => 'DC',
      'DELAWARE' => 'DE',
      'FLORIDA' => 'FL',
      'GEORGIA' => 'GA',
      'HAWAII' => 'HI',
      'IOWA' => 'IA',
      'IDAHO' => 'ID',
      'ILLINOIS' => 'IL',
      'INDIANA' => 'IN',
      'KANSAS' => 'KS',
      'KENTUCKY' => 'KY',
      'LOUISIANA' => 'LA',
      'MANITOBA' => 'MB',
      'MASSACHUSETTS' => 'MA',
      'MARYLAND' => 'MD',
      'MAINE' => 'ME',
      'MICHIGAN' => 'MI',
      'MINNESOTA' => 'MN',
      'MISSOURI' => 'MO',
      'MISSISSIPPI' => 'MS',
      'MONTANA' => 'MT',
      'NORTH CAROLINA' => 'NC',
      'NORTH DAKOTA' => 'ND',
      'NEBRASKA' => 'NE',
      'NEW BRUNSWICK' => 'NB',
      'NEWFOUNDLAND' => 'NL',
      'NEW HAMPSHIRE' => 'NH',
      'NEW JERSEY' => 'NJ',
      'NEW MEXICO' => 'NM',
      'NEVADA' => 'NV',
      'NEW YORK' => 'NY',
      'NOVA SCOTIA' => 'NS',
      'NORTHWEST TERRITORIES' => 'NT',
      'NUNAVUT' => 'NU',
      'OHIO' => 'OH',
      'OKLAHOMA' => 'OK',
      'ONTARIO' => 'ON',
      'OREGON' => 'OR',
      'PENNSYLVANIA' => 'PA',
      'PRINCE EDWARD ISLAND' => 'PE',
      'QUEBEC' => 'QC',
      'RHODE ISLAND' => 'RI',
      'SASKATCHEWAN' => 'SK',
      'SOUTH CAROLINA' => 'SC',
      'SOUTH DAKOTA' => 'SD',
      'TENNESSEE' => 'TN',
      'TEXAS' => 'TX',
      'UTAH' => 'UT',
      'VIRGINIA' => 'VA',
      'VERMONT' => 'VT',
      'WASHINGTON' => 'WA',
      'WISCONSIN' => 'WI',
      'WEST VIRGINIA' => 'WV',
      'WYOMING' => 'WY',
      'YUKON' => 'YT'
    );
    //ORIGINAL, Just mastercard and Visa.  Copied in next section from cc_validation.php
    //NEW section for identifying credit cards, does all the cc_validation handles.
    if (ereg('^4[0-9]{12}([0-9]{3})?$', $this->cc_card_number) and CC_ENABLED_VISA == '1')
     {
      $card_type = "001";
     }
    elseif (ereg('^5[1-5][0-9]{14}$', $this->cc_card_number) and CC_ENABLED_MC == '1')
     {
      $card_type = "002";
     }
    elseif (ereg('^3[47][0-9]{13}$', $this->cc_card_number) and CC_ENABLED_AMEX == '1')
     {
      $card_type = "003";
     }
    elseif (ereg('^3(0[0-5]|[68][0-9])[0-9]{11}$', $this->cc_card_number) and CC_ENABLED_DINERS_CLUB == '1')
     {
      $card_type = "005";
     }
    elseif (ereg('^6011[0-9]{12}$', $this->cc_card_number) and CC_ENABLED_DISCOVER == '1')
     {
      $card_type = "004";
     }
    elseif (ereg('^(3[0-9]{4}|2131|1800)[0-9]{11}$', $this->cc_card_number) and CC_ENABLED_JCB == '1')
     {
      $card_type = "007";
     } //otherwise anything is possible.
    // The line below may be a duplicate, but the first instance above was
    // already included_once, so this shouldn't be an issue and can be left
    // alone.
    //include_once("HOP.php");
    //include_once(DIR_FS_CATALOG. DIR_WS_MODULES . 'payment/cybersource/HOP.php');
    // Set this to whatever your want your prefix to be...it'll help you to
    // eyeball what orders are from your Zen-Cart when using your CycleSource
    // back-end. Set to '' to make it empty, so just the order number shows.
    define('HMAC_SHA256', 'sha256');
    define('SECRET_KEY', MODULE_PAYMENT_CS_SECRET_KEY);
    function sign($params)
     {
      return signData(buildDataToSign($params), SECRET_KEY);
     }
    function signData($data, $secretKey)
     {
      return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
     }
    function buildDataToSign($params)
     {
      $signedFieldNames = explode(",", $params["signed_field_names"]);
      foreach ($signedFieldNames as $field)
       {
        $dataToSign[] = $field . "=" . $params[$field];
       }
      return commaSeparate($dataToSign);
     }
    function commaSeparate($dataToSign)
     {
      return implode(",", $dataToSign);
     }
    #END NEW CODE GREG
    $cardexpirydate = $this->cc_expiry_month . "-" . $this->cc_expiry_year;
    $signeddatetime = gmdate('Y-m-d H:i:s');
    $signeddatetime = gmdate("Y-m-d\TH:i:s\Z");
    $amount         = number_format($order->info['total'], 2, '.', '');
    if (!isset($amount))
     {
      $amount = "0.00";
     }
    if (!isset($currency))
     {
      $currency = "USD";
     }
    if (!isset($_SESSION['unqtrans']) || $_SESSION['unqtrans'] == '')
     {
      $transir              = uniqid();
      $_SESSION['unqtrans'] = $transir;
     }
    else
     {
      $transir = $_SESSION['unqtrans'];
     }
    $searchph           = array(
      '(',
      ')',
      '-',
      '+',
      '{',
      '}',
      ' '
    );
    $replaceph          = array(
      '',
      '',
      '',
      '',
      '',
      '',
      ''
    );
    
		if (!isset($order->billing['suburb'])) {
			$order->billing['suburb'] = '';
		}
		
    //Used in signature
    $paytypecybersource = strtolower(MODULE_PAYMENT_CS_TYPE);
    if (!$paytypecybersource)
      $paytypecybersource = "authorization";
    $params = array(
      'transaction_type' => $paytypecybersource,
      'access_key' => MODULE_PAYMENT_CS_ACCESS_KEY,
      'profile_id' => MODULE_PAYMENT_CS_PROFILE_ID,
      'amount' => $amount,
      'currency' => $currency,
      'signed_field_names' => 'transaction_type,access_key,profile_id,amount,currency,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_uuid,reference_number,payment_method,card_type,tax_amount,card_number,card_expiry_date,bill_to_forename,bill_to_surname,bill_to_address_line1,bill_to_address_line2,bill_to_address_city,bill_to_address_state,bill_to_address_postal_code,bill_to_address_country,bill_to_phone,bill_to_email',
      'unsigned_field_names' => '',
      'signed_date_time' => $signeddatetime,
      'locale' => 'en',
      'transaction_uuid' => $transir,
      'reference_number' => MODULE_PAYMENT_CS_ORDER_PREFIX . $new_order_id,
      'payment_method' => 'card',
      'card_type' => $this->cc_card_type_num,
      'tax_amount' => number_format($order->info['tax'], 2),
      'card_number' => $this->cc_card_number,
      'card_expiry_date' => $cardexpirydate,
      'bill_to_forename' => $this->cc_card_first_name,
      'bill_to_surname' => $this->cc_card_last_name,
      'bill_to_address_line1' => $order->billing['street_address'],
      'bill_to_address_line2' => $order->billing['suburb'],
      'bill_to_address_city' => $order->billing['city'],
      'bill_to_address_state' => $states[strtoupper($order->billing['state'])],
      'bill_to_address_postal_code' => $order->billing['postcode'],
      'bill_to_address_country' => $order->billing['country']['iso_code_2'],
      'bill_to_phone' => str_replace($searchph, $replaceph, $order->customer['telephone']),
      'bill_to_email' => $order->customer['email_address']
    );
	$params2 = array(
      'transaction_type' => $paytypecybersource,
      'access_key' => MODULE_PAYMENT_CS_ACCESS_KEY,
      'profile_id' => MODULE_PAYMENT_CS_PROFILE_ID,
      'amount' => $amount,
      'currency' => $currency,
      'signed_field_names' => 'transaction_type,access_key,profile_id,amount,currency,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_uuid,reference_number,payment_method,card_type,tax_amount,card_number,card_expiry_date,bill_to_forename,bill_to_surname,bill_to_address_line1,bill_to_address_line2,bill_to_address_city,bill_to_address_state,bill_to_address_postal_code,bill_to_address_country,bill_to_phone,bill_to_email',
      'unsigned_field_names' => '',
      'signed_date_time' => $signeddatetime,
      'locale' => 'en',
      'transaction_uuid' => $transir,
      'reference_number' => MODULE_PAYMENT_CS_ORDER_PREFIX . $new_order_id,
      'payment_method' => 'card',
      'card_type' => $this->cc_card_type_num,
      'tax_amount' => number_format($order->info['tax'], 2),
      'card_number' => str_repeat('X', (strlen($this->cc_card_number) - 4)) . substr($this->cc_card_number, -4),
      'card_expiry_date' => $cardexpirydate,
      'bill_to_forename' => $this->cc_card_first_name,
      'bill_to_surname' => $this->cc_card_last_name,
      'bill_to_address_line1' => $order->billing['street_address'],
      'bill_to_address_line2' => $order->billing['suburb'],
      'bill_to_address_city' => $order->billing['city'],
      'bill_to_address_state' => $states[strtoupper($order->billing['state'])],
      'bill_to_address_postal_code' => $order->billing['postcode'],
      'bill_to_address_country' => $order->billing['country']['iso_code_2'],
      'bill_to_phone' => str_replace($searchph, $replaceph, $order->customer['telephone']),
      'bill_to_email' => $order->customer['email_address']
    );
	
	
    $process_button_string .= zen_draw_hidden_field('transaction_type', $paytypecybersource) . "\n" . zen_draw_hidden_field('access_key', MODULE_PAYMENT_CS_ACCESS_KEY) . "\n" . zen_draw_hidden_field('profile_id', MODULE_PAYMENT_CS_PROFILE_ID) . "\n" . zen_draw_hidden_field('amount', $amount) . "\n" . zen_draw_hidden_field('currency', $currency) . "\n" . zen_draw_hidden_field('signed_field_names', 'transaction_type,access_key,profile_id,amount,currency,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_uuid,reference_number,payment_method,card_type,tax_amount,card_number,card_expiry_date,bill_to_forename,bill_to_surname,bill_to_address_line1,bill_to_address_line2,bill_to_address_city,bill_to_address_state,bill_to_address_postal_code,bill_to_address_country,bill_to_phone,bill_to_email') . "\n" . zen_draw_hidden_field('unsigned_field_names', '') . "\n" . zen_draw_hidden_field('signed_date_time', $signeddatetime) . "\n" . zen_draw_hidden_field('locale', 'en') . "\n" . zen_draw_hidden_field('transaction_uuid', $transir) . "\n" . zen_draw_hidden_field('reference_number', MODULE_PAYMENT_CS_ORDER_PREFIX . $new_order_id) . "\n" . zen_draw_hidden_field('payment_method', 'card') . "\n" . zen_draw_hidden_field('card_type', $card_type) . "\n" . zen_draw_hidden_field('tax_amount', number_format($order->info['tax'], 2)) . "\n" . zen_draw_hidden_field('card_number', $this->cc_card_number) . "\n" . zen_draw_hidden_field('card_expiry_date', $cardexpirydate) . "\n" . zen_draw_hidden_field('bill_to_forename', $this->cc_card_first_name) . "\n" . zen_draw_hidden_field('bill_to_surname', $this->cc_card_last_name) . "\n" . zen_draw_hidden_field('bill_to_address_line1', $order->billing['street_address']) . "\n" . zen_draw_hidden_field('bill_to_address_line2', $order->billing['suburb']) . "\n" . zen_draw_hidden_field('bill_to_address_city', $order->billing['city']) . "\n" . 
    // MODIFIED BY NUMINIX TO NOT USE ABBREVIATIONS IF THEY DO NOT EXIST	
      zen_draw_hidden_field('bill_to_address_state', $states[strtoupper($order->billing['state'])]) . "\n" . zen_draw_hidden_field('bill_to_address_postal_code', $order->billing['postcode']) . "\n" . zen_draw_hidden_field('bill_to_address_country', $order->billing['country']['iso_code_2']) . "\n" . zen_draw_hidden_field('bill_to_phone', str_replace($searchph, $replaceph, $order->customer['telephone'])) . "\n" . zen_draw_hidden_field('bill_to_email', $order->customer['email_address']) . "\n" . zen_draw_hidden_field('signature', sign($params)) . "\n";
	  
	  $process_button_string2 .= zen_draw_hidden_field('transaction_type', $paytypecybersource) . "\n" . zen_draw_hidden_field('access_key', MODULE_PAYMENT_CS_ACCESS_KEY) . "\n" . zen_draw_hidden_field('profile_id', MODULE_PAYMENT_CS_PROFILE_ID) . "\n" . zen_draw_hidden_field('amount', $amount) . "\n" . zen_draw_hidden_field('currency', $currency) . "\n" . zen_draw_hidden_field('signed_field_names', 'transaction_type,access_key,profile_id,amount,currency,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_uuid,reference_number,payment_method,card_type,tax_amount,card_number,card_expiry_date,bill_to_forename,bill_to_surname,bill_to_address_line1,bill_to_address_line2,bill_to_address_city,bill_to_address_state,bill_to_address_postal_code,bill_to_address_country,bill_to_phone,bill_to_email') . "\n" . zen_draw_hidden_field('unsigned_field_names', '') . "\n" . zen_draw_hidden_field('signed_date_time', $signeddatetime) . "\n" . zen_draw_hidden_field('locale', 'en') . "\n" . zen_draw_hidden_field('transaction_uuid', $transir) . "\n" . zen_draw_hidden_field('reference_number', MODULE_PAYMENT_CS_ORDER_PREFIX . $new_order_id) . "\n" . zen_draw_hidden_field('payment_method', 'card') . "\n" . zen_draw_hidden_field('card_type', $card_type) . "\n" . zen_draw_hidden_field('tax_amount', number_format($order->info['tax'], 2)) . "\n" . zen_draw_hidden_field('card_number', str_repeat('X', (strlen($this->cc_card_number) - 4)) . substr($this->cc_card_number, -4)) . "\n" . zen_draw_hidden_field('card_expiry_date', $cardexpirydate) . "\n" . zen_draw_hidden_field('bill_to_forename', $this->cc_card_first_name) . "\n" . zen_draw_hidden_field('bill_to_surname', $this->cc_card_last_name) . "\n" . zen_draw_hidden_field('bill_to_address_line1', $order->billing['street_address']) . "\n" . zen_draw_hidden_field('bill_to_address_line2', $order->billing['suburb']) . "\n" . zen_draw_hidden_field('bill_to_address_city', $order->billing['city']) . "\n" . 
    // MODIFIED BY NUMINIX TO NOT USE ABBREVIATIONS IF THEY DO NOT EXIST	
      zen_draw_hidden_field('bill_to_address_state', $states[strtoupper($order->billing['state'])]) . "\n" . zen_draw_hidden_field('bill_to_address_postal_code', $order->billing['postcode']) . "\n" . zen_draw_hidden_field('bill_to_address_country', $order->billing['country']['iso_code_2']) . "\n" . zen_draw_hidden_field('bill_to_phone', str_replace($searchph, $replaceph, $order->customer['telephone'])) . "\n" . zen_draw_hidden_field('bill_to_email', $order->customer['email_address']) . "\n" . zen_draw_hidden_field('signature', sign($params)) . "\n";
	  
if(MODULE_PAYMENT_CS_DEBUG == "Yes"){
	$signeddataset = "Signed Data \n";
	foreach ($params2 as $keyset => $valueset) {
    $signeddataset .=  "Post Name: $keyset Value: $valueset \n";
}

	$signeddataset .= "\n\n Posted Data To Cybersource \n";
	$signeddataset .= $process_button_string2;
	$fileopenset = DIR_FS_CATALOG . 'logs/cybersource/cs_' . $this->setfiletime .'.log';
	$fp = fopen($fileopenset, 'w');
	fwrite($fp, $signeddataset); 
	fclose($fp);
	$_SESSION['csdebugtime'] = $this->setfiletime;
	}
	  
	  
	  
	  
    return $process_button_string;
   }
  function before_process()
   {
    global $_GET, $messageStack, $order, $ccowner;
    $_SESSION['unqtrans'] = '';
    if (isset($_POST['decision']) && $_POST['decision'] == "ACCEPT")
     {
$setfiletimeset = $_SESSION['csdebugtime'];
$fileopenset = DIR_FS_CATALOG . 'logs/cybersource/cs_' . $setfiletimeset .'.log';
if (file_exists($fileopenset) && MODULE_PAYMENT_CS_DEBUG == "Yes"){

$signeddataset = "\n\n Retuned Post Data From Cyborsource \n";
foreach ($_POST as $keyset => $valueset) {
    $signeddataset .=  "Post Name: $keyset Value: $valueset \n";
}
$handle = fopen($fileopenset, "a");

fwrite($handle, $signeddataset); // write it
fclose($handle); 
}
	 
	 
	 
      $getccexpire               = substr($_POST['req_card_expiry_date'], 0, 2) . '' . substr($_POST['req_card_expiry_date'], -2);
      $order->info['cc_expires'] = $getccexpire;
      $order->info['cc_owner']   = $_POST['req_bill_to_forename'] . ' ' . $_POST['req_bill_to_surname'];
      switch ($_POST['req_card_type'])
      {
        case "002":
          $order->info['cc_type'] = "MasterCard";
          break;
        case "001":
          $order->info['cc_type'] = "Visa";
          break;
        case "003":
          $order->info['cc_type'] = "American Express";
        case "004":
          $order->info['cc_type'] = "Discover";
          break;
      }
      $order->info['cc_number'] = $_POST['req_card_number'];
      return;
     }
    else // These are the real reason codes that I thought were most descriptive.
     {
      // Feel free to add/remove the codes you want, but these should
      // be all you'll need.
      if (strcmp($_POST['reason_code'], '150') == 0 || strcmp($_POST['reason_code'], '207') == 0 || strcmp($_POST['reason_code'], '236') == 0)
       {
        $error = "There was a general error. Please try again in a few minutes.";
       }
      elseif (strcmp($_POST['reason_code'], '151') == 0 || strcmp($_POST['reason_code'], '152') == 0 || strcmp($_POST['reason_code'], '250') == 0)
       {
        $error = "There was a general error. Please contact us to avoid any duplication of your order.";
       }
      elseif (strcmp($_POST['reason_code'], '202') == 0)
       {
        $error = "Your credit card has expired. Please use another card.";
       }
      elseif (strcmp($_POST['reason_code'], '204') == 0)
       {
        $error = "There are insufficient funds in that card. Please try another card.";
       }
      elseif (strcmp($_POST['reason_code'], '205') == 0)
       {
        $error = "This card has been reported lost or stolen. Please destroy it and use another card.";
       }
      elseif (strcmp($_POST['reason_code'], '208') == 0 || strcmp($_POST['reason_code'], '220') == 0)
       {
        $error = "This card is not authorized for card-not-present transactions. Please use another card.";
       }
      elseif (strcmp($_POST['reason_code'], '210') == 0)
       {
        $error = "This card is maxed out. Please use another card.";
       }
      elseif (strcmp($_POST['reason_code'], '211') == 0)
       {
        $error = "Invalid card verification number. Please use another card.";
       }
      elseif (strcmp($_POST['reason_code'], '222') == 0)
       {
        $error = "Your account is frozen. Please use another card.";
       }
      elseif (strcmp($_POST['reason_code'], '231') == 0)
       {
        $error = "The card number you entered is invalid. Please use another card.";
       }
      elseif (strcmp($_POST['reason_code'], '520') == 0)
       {
        $error = "Your billing address, name, or CVV number does not match the card number provided. Please correct the information and try again.";
       }
      elseif (strcmp($_POST['reason_code'], '') == 0)
       {
        $error = "There was a general error. Please double-check your information and try again.";
       }
      elseif (strcmp($_POST['reason_code'], '102') == 0)
       {
        $error = "There was an error in your data. Please double-check for Special Characters in your information and try again.";
       }
      else
       {
        $error = "There was a general error. Please contact us to avoid order duplication. Error #" . $_POST['reason_code'];
       }
      //die($error);
      $messageStack->add_session('checkout_payment', $error, 'error');
      zen_redirect(zen_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL', true, false));
     }
   }
  function after_process()
   {
    global $insert_id, $db, $_POST, $order, $ccowner;
    $db->execute("update " . TABLE_ORDERS . " set cc_cvv ='" . $_SESSION['cardcvnumber'] . "' where orders_id = '" . $insert_id . "'");
   }
  function check()
   {
    global $db;
    if (!isset($this->_check))
     {
      $check_query  = $db->Execute("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_PAYMENT_CS_STATUS'");
      $this->_check = $check_query->RecordCount();
     }
    return $this->_check;
   }
  function install()
   {
    global $db;
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable CyberSource HOP Module', 'MODULE_PAYMENT_CS_STATUS', 'True', 'Do you want to accept VISA or MC 
			through CyberSource?', '6', '1', 'zen_cfg_select_option(array(\'True\', \'False\'), ', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Login Username', 'MODULE_PAYMENT_CS_LOGIN', 'testing', 'The login username used for CyberSource', '6', '2', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_description, configuration_group_id, sort_order, last_modified) VALUES ('Profile ID', 'MODULE_PAYMENT_CS_PROFILE_ID', 'This is Your Profile ID you created at Cybersource.', '6', '3', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Secret Key', 'MODULE_PAYMENT_CS_SECRET_KEY', 'This is the key associated with Profile ID & is Valid For 2 Years. <strong>Make sure it\'s all on one line.</strong>', '6', '4', now(), now(), NULL, 'zen_cfg_textarea(')");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_description, configuration_group_id, sort_order, last_modified, date_added) VALUES ('Access Key', 'MODULE_PAYMENT_CS_ACCESS_KEY', 'This is the Access key associated with Profile ID.', '6', '5', now(), now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Authenticate or Sale', 'MODULE_PAYMENT_CS_TYPE', 'Authorization', 'Do you want to just Authorize the Transaction (Authorization) or Authorize & Get Payment(Sale)?', '6', '6', 'zen_cfg_select_option(array(\'Authorization\', \'Sale\'), ', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Transaction Mode', 'MODULE_PAYMENT_CS_TESTMODE', 'Test', 'Transaction mode used for processing orders', '6', '7', 'zen_cfg_select_option(array(\'Test\', \'Production\'), ', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Transaction Method', 'MODULE_PAYMENT_CS_METHOD', 'Credit Card', 'Transaction method used for processing orders', '6', '8', 'zen_cfg_select_option(array(\'Credit Card\'), ', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Collect & store the CVV number', 'MODULE_PAYMENT_CS_COLLECT_CVV', 'True', 'Do you want to collect the CVV number. Note: If you do the CVV number will be stored in the database in an encoded format.', '6', '9', 'zen_cfg_select_option(array(\'True\', \'False\'), ', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort order of display.', 'MODULE_PAYMENT_CS_SORT_ORDER', '0', 'Sort order of display. Lowest is displayed first.', '6', '10', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Payment Zone', 'MODULE_PAYMENT_CS_ZONE', '0', 'If a zone is selected, only enable this payment method for that zone.', '6', '11', 'zen_get_zone_class_title', 'zen_cfg_pull_down_zone_classes(', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, use_function, date_added) values ('Set Order Status', 'MODULE_PAYMENT_CS_ORDER_STATUS_ID', '0', 'Set the status of orders made with this payment module to this value', '6', '12', 'zen_cfg_pull_down_order_statuses(', 'zen_get_order_status_name', now())");
	$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Module Records', 'MODULE_PAYMENT_CS_DEBUG', 'No', 'Do you want to Save Sent & Received Post Transation Records To /logs/cybersource/ Folder?', '6', '13', 'zen_cfg_select_option(array(\'Yes\', \'No\'), ', now())");
   }
  function remove()
   {
    global $db;
    $db->Execute("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
   }
  function keys()
   {
    return array(
      'MODULE_PAYMENT_CS_STATUS',
      'MODULE_PAYMENT_CS_LOGIN',
      'MODULE_PAYMENT_CS_PROFILE_ID',
      'MODULE_PAYMENT_CS_SECRET_KEY',
      'MODULE_PAYMENT_CS_ACCESS_KEY',
      'MODULE_PAYMENT_CS_TYPE',
      'MODULE_PAYMENT_CS_TESTMODE',
      'MODULE_PAYMENT_CS_METHOD',
      'MODULE_PAYMENT_CS_COLLECT_CVV',
      'MODULE_PAYMENT_CS_ZONE',
      'MODULE_PAYMENT_CS_ORDER_STATUS_ID',
      'MODULE_PAYMENT_CS_SORT_ORDER',
	 'MODULE_PAYMENT_CS_DEBUG'
    );
   }
 }
?>