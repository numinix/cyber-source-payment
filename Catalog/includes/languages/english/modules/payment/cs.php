<?php
//
// +----------------------------------------------------------------------+
// |zen-cart Open Source E-commerce                                       |
// +----------------------------------------------------------------------+
// | Copyright (c) 2003 The zen-cart developers                           |
// |                                                                      |   
// | http://www.zen-cart.com/index.php                                    |   
// |                                                                      |   
// | Portions Copyright (c) 2003 osCommerce                               |
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.zen-cart.com/license/2_0.txt.                             |
// | If you did not receive a copy of the zen-cart license and are unable |
// | to obtain it through the world-wide-web, please send a note to       |
// | license@zen-cart.com so we can mail you a copy immediately.          |
// +----------------------------------------------------------------------+
//

  define('MODULE_PAYMENT_CS_TEXT_TITLE', 'Credit Card - Cyboursource');
  define('MODULE_PAYMENT_CS_TEXT_DESCRIPTION', 'CyberSource Credit Card Authorization Service');
  define('MODULE_PAYMENT_CS_TEXT_TYPE', 'Type:');
  define('MODULE_PAYMENT_CS_TEXT_CREDIT_CARD_OWNER_FIRST', 'Card Owner\'s First Name:');
  define('MODULE_PAYMENT_CS_TEXT_CREDIT_CARD_OWNER_LAST', 'Card Owner\'s Last Name:');
  define('MODULE_PAYMENT_CS_TEXT_CREDIT_CARD_NUMBER', 'Credit Card Number:');
  define('MODULE_PAYMENT_CS_TEXT_CREDIT_CARD_EXPIRES', 'Credit Card Expiry Date:');
  define('MODULE_PAYMENT_CS_TEXT_CREDIT_CARD_CVV', 'CVV Number (<a href="javascript:popupWindow(\'' . zen_href_link(FILENAME_POPUP_CVV_HELP) . '\')">' . 'More Info' . '</a>)');
  define('MODULE_PAYMENT_CS_TEXT_JS_CC_OWNER', '* The owner\'s name of the credit card must be at least ' . CC_OWNER_MIN_LENGTH . ' characters.\n');
  define('MODULE_PAYMENT_CS_TEXT_JS_CC_NUMBER', '* The credit card number must be at least ' . CC_NUMBER_MIN_LENGTH . ' characters.\n');
  define('MODULE_PAYMENT_CS_TEXT_JS_CC_CVV', '* The CVV number must be at least ' . CC_CVV_MIN_LENGTH . ' characters.\n');
  define('MODULE_PAYMENT_CS_TEXT_ERROR_MESSAGE', 'There has been an error processing your credit card. Please try again.');
  define('MODULE_PAYMENT_CS_TEXT_DECLINED_MESSAGE', 'Your credit card was declined. Please try another card or contact your bank for more info.');
  define('MODULE_PAYMENT_CS_TEXT_ERROR', 'Credit Card Error!');
  //You must change this prefix if you are using 1 bank account and multiple stores, when confirming an authorization this would appear:
  // Order Information: Order #: <zencart order #>
  define('MODULE_PAYMENT_CS_ORDER_PREFIX', 'Order #');
  define('MODULE_PAYMENT_CS_TEXT_FEC_CC_CVV', '* The CVV number must be at least ' . CC_CVV_MIN_LENGTH . ' characters.');
  define('MODULE_PAYMENT_CS_TEXT_FEC_CC_NUMBER', '* The credit card number must be at least ' . CC_NUMBER_MIN_LENGTH . ' characters.');
  define('MODULE_PAYMENT_CS_TEXT_FEC_CC_OWNER', '* The owner\'s name of the credit card must be at least ' . CC_OWNER_MIN_LENGTH . ' characters.');
?>